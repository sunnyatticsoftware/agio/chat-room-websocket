namespace Chat.Domain.Contracts;

public interface IDateTimeFactory
{
    /// <summary>
    /// Retrieves the current UTC date
    /// </summary>
    /// <returns>the current UTC date</returns>
    DateTime CreateUtcNow();
}