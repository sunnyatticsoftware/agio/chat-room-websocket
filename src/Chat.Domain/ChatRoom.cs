using Chat.Domain.Contracts;

namespace Chat.Domain;

public class ChatRoom
{
    private readonly IDateTimeFactory _dateTimeFactory;
    private readonly INotificationService _notificationService;
    private readonly IList<User> _users = new List<User>();
    private readonly IList<Message> _messages = new List<Message>();

    public Guid Id { get; }
    
    public ChatRoom(Guid id, IDateTimeFactory dateTimeFactory, INotificationService notificationService)
    {
        Id = id;
        _dateTimeFactory = dateTimeFactory;
        _notificationService = notificationService;
    }

    public void Join(User user)
    {
        var isSameUser = _users.Contains(user);
        if (isSameUser)
        {
            throw new ArgumentException($"User with username {user.Username} and connection Id {user.ConnectionId} already exists");
        }
        
        var isExisting = IsUserJoined(user.Username);
        if (isExisting)
        {
            _users.Remove(_users.Single(x => x.Username == user.Username));
        }
        
        _users.Add(user);
    }

    public async Task SendMessage(string username, string text)
    {
        var isExisting = IsUserJoined(username);
        if (!isExisting)
        {
            throw new ArgumentException($"User with username {username} not joined");
        }

        var createdOn = _dateTimeFactory.CreateUtcNow();
        var connectionIds = _users.Select(x => x.ConnectionId);
        var message = new Message(createdOn, username, text);

        var tasks = new List<Task>();
        foreach (var connectionId in connectionIds)
        {
            var task = _notificationService.SendNotification(connectionId, message.ToString());
            tasks.Add(task);
        }
        
        await Task.WhenAll(tasks);
        _messages.Add(message);
    }

    private bool IsUserJoined(string username)
    {
        var isUserJoined = _users.Any(x => x.Username == username);
        return isUserJoined;
    }

    public IEnumerable<Message> GetAllMessages()
    {
        return _messages;
    }
}