using Chat.Application.Contracts;
using Chat.Domain;

namespace Chat;

public class InMemoryChatRoomRepository
    : IChatRoomRepository
{
    private readonly IDictionary<Guid, ChatRoom> _chatRooms = new Dictionary<Guid, ChatRoom>();
    
    public Task<ChatRoom> GetById(Guid id)
    {
        var isFound = _chatRooms.TryGetValue(id, out var chatRoom);
        if (!isFound || chatRoom is null)
        {
            throw new KeyNotFoundException($"Could not find chat room with Id {id}");
        }

        return Task.FromResult(chatRoom);
    }

    public Task Save(ChatRoom chatRoom)
    {
        _chatRooms[chatRoom.Id] = chatRoom;
        return Task.CompletedTask;
    }
}