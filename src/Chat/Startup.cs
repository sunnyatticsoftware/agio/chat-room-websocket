using Chat.Application;
using Chat.Application.Contracts;
using Chat.Domain.Contracts;
using Chat.Extensions;

namespace Chat;

public class Startup
{
    public void ConfigureServices(IServiceCollection services)
    {
        services
            .AddSingleton<IDateTimeFactory, DateTimeFactory>()
            .AddTransient<ChatCommandService>()
            .AddTransient<ChatQueryService>()
            .AddSingleton<IChatRoomRepository, InMemoryChatRoomRepository>()
            .AddSingleton<IChatRoomFactory, ChatRoomFactory>()
            .AddSingleton<IIdFactory, IdFactory>()
            .AddTransient<INotificationService>(sp => new NotificationService(NotificationServiceConfiguration.Default))
            .AddOpenApi()
            .AddControllers();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        app.UseRouting();
        app.UseOpenApi();
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    }
}