using Chat.Domain.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Chat.Controllers;

[Route("[controller]")]
[AllowAnonymous]
public class HealthController
    : ControllerBase
{
    private readonly IWebHostEnvironment _environment;
    private readonly IDateTimeFactory _dateTimeFactory;

    public HealthController(
        IWebHostEnvironment environment,
        IDateTimeFactory dateTimeFactory)
    {
        _environment = environment;
        _dateTimeFactory = dateTimeFactory;
    }
        
    /// <summary>
    /// Gets service's health
    /// </summary>
    [HttpGet]
    public IActionResult Get()
    {
        var environmentName = _environment.EnvironmentName;
        var currentUtcDateTime = _dateTimeFactory.CreateUtcNow().ToString("yyyy-MM-dd HH:mm:ss");
        var assemblyName = typeof(Startup).Assembly.GetName();
        var result = $"Assembly={assemblyName}, Environment={environmentName}, CurrentUtcTime={currentUtcDateTime}";
        return Ok(result);
    }
}