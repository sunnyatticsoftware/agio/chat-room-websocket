using Chat.Application.Contracts;

namespace Chat.Application;

public class IdFactory
    : IIdFactory
{
    public Guid Create()
    {
        return Guid.NewGuid();
    }
}