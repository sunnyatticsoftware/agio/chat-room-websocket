using Chat.Domain;

namespace Chat.Application.Contracts;

public interface IChatRoomFactory
{
    ChatRoom Create();
}