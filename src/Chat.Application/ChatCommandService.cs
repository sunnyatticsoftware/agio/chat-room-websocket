using Chat.Application.Contracts;
using Chat.Application.Models;
using Chat.Domain;

namespace Chat.Application;

public class ChatCommandService
{
    private readonly IChatRoomRepository _chatRoomRepository;
    private readonly IChatRoomFactory _chatRoomFactory;

    public ChatCommandService(
        IChatRoomRepository chatRoomRepository,
        IChatRoomFactory chatRoomFactory)
    {
        _chatRoomRepository = chatRoomRepository;
        _chatRoomFactory = chatRoomFactory;
    }

    public async Task<Guid> Create()
    {
        var chatRoom = _chatRoomFactory.Create();
        var id = chatRoom.Id;
        await _chatRoomRepository.Save(chatRoom);
        return id;
    }

    public async Task Join(Guid chatRoomId, UserDto userDto)
    {
        var chatRoom = await _chatRoomRepository.GetById(chatRoomId);
        var user = new User(userDto.Username, userDto.ConnectionId);
        chatRoom.Join(user);
        await _chatRoomRepository.Save(chatRoom);
    }

    public async Task SendMessage(Guid chatRoomId, string username, string text)
    {
        var chatRoom = await _chatRoomRepository.GetById(chatRoomId);
        await chatRoom.SendMessage(username, text);
        await _chatRoomRepository.Save(chatRoom);
    }
}