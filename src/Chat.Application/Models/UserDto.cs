namespace Chat.Application.Models;

public class UserDto
{
    public string Username { get; }
    public string ConnectionId { get; }

    public UserDto(string username, string connectionId)
    {
        Username = username;
        ConnectionId = connectionId;
    }
}