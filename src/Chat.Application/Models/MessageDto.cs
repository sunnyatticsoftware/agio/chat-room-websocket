namespace Chat.Application.Models;

public record MessageDto(DateTime CreatedOn, string CreatedBy, string Text, string FormattedText);