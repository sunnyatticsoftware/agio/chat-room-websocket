using Chat.Application.Contracts;
using Chat.Application.Models;

namespace Chat.Application;

public class ChatQueryService
{
    private readonly IChatRoomRepository _chatRoomRepository;

    public ChatQueryService(IChatRoomRepository chatRoomRepository)
    {
        _chatRoomRepository = chatRoomRepository;
    }

    public async Task<IEnumerable<MessageDto>> GetAllMessages(Guid chatRoomId)
    {
        var chatRoom = await _chatRoomRepository.GetById(chatRoomId);
        var messages = chatRoom.GetAllMessages();
        var messageDtos = messages.Select(x => new MessageDto( x.CreatedOn, x.CreatedBy, x.Text , x.ToString()));
        return messageDtos;
    }
}