using Chat.Application.Contracts;
using Chat.Application.Models;
using Chat.Application.UnitTests.TestSupport;
using Chat.Application.UnitTests.TestSupport.Builders;
using Chat.Application.UnitTests.TestSupport.Extensions;
using Chat.Domain;
using Chat.Domain.Contracts;
using FluentAssertions;
using Moq;

namespace Chat.Application.UnitTests.ChatCommandServiceTests;

public static class JoinTests
{
    public class Should_Join_A_User
        : Given_When_Then_Async
    {
        private Mock<IChatRoomRepository> _chatRoomRepositoryMock = null!;
        private ChatCommandService _sut = null!;
        private Exception _exception = null!;
        private Guid _chatRoomId;
        private UserDto _user = null!;

        protected override Task Given()
        {
            _chatRoomId = 1.ToGuid();
            _user = new UserDto("foo", "bar");
            
            var chatRoom = new ChatRoom(_chatRoomId, Mock.Of<IDateTimeFactory>(), Mock.Of<INotificationService>());

            var chatRoomFactoryMock = new Mock<IChatRoomFactory>();
            chatRoomFactoryMock
                .Setup(x => x.Create())
                .Returns(chatRoom);
            
            _chatRoomRepositoryMock = new Mock<IChatRoomRepository>();
            _chatRoomRepositoryMock
                .Setup(x => x.GetById(_chatRoomId))
                .ReturnsAsync(chatRoom);
            
            _sut =
                new ChatCommandServiceBuilder()
                    .WithChatRoomRepository(_chatRoomRepositoryMock.Object)
                    .WithChatRoomFactory(chatRoomFactoryMock.Object)
                    .Object;

            return Task.CompletedTask;
        }

        protected override async Task When()
        {
            try
            {
                await _sut.Join(_chatRoomId, _user);
            }
            catch (Exception exception)
            {
                _exception = exception;
            }
        }

        [Fact]
        public void Then_It_Should_Not_Throw_Any_Exception()
        {
            _exception.Should().BeNull();
        }

        [Fact]
        public void Then_It_Should_Save_The_Same_Chat_Room_That_Was_Retrieved()
        {
            _chatRoomRepositoryMock.Verify(x => x.Save(It.Is<ChatRoom>(chatRoom => chatRoom.Id == _chatRoomId)), Times.Once);
        }
    }
}