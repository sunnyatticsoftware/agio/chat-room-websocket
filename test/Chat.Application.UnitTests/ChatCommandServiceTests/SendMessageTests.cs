using Chat.Application.Contracts;
using Chat.Application.UnitTests.TestSupport;
using Chat.Application.UnitTests.TestSupport.Builders;
using Chat.Application.UnitTests.TestSupport.Extensions;
using Chat.Domain;
using Chat.Domain.Contracts;
using FluentAssertions;
using Moq;

namespace Chat.Application.UnitTests.ChatCommandServiceTests;

public static class SendMessageTests
{
    public class Should_Send_Message
        : Given_When_Then_Async
    {
        private Guid _chatRoomId;
        private ChatCommandService _sut = null!;
        private Exception _exception = null!;
        private string _username = null!;
        private string _message = null!;

        protected override Task Given()
        {
            _chatRoomId = 1.ToGuid();
            _username = "foo";
            _message = "hello";
            
            var chatRoom = new ChatRoom(_chatRoomId, Mock.Of<IDateTimeFactory>(), Mock.Of<INotificationService>());
            
            var userOne = new User(_username, "connection-one");
            chatRoom.Join(userOne);
            
            var chatRoomRepositoryMock = new Mock<IChatRoomRepository>();
            chatRoomRepositoryMock
                .Setup(x => x.GetById(_chatRoomId))
                .ReturnsAsync(chatRoom);

            _sut =
                new ChatCommandServiceBuilder()
                    .WithChatRoomRepository(chatRoomRepositoryMock.Object)
                    .Object;
            
            return Task.CompletedTask;
        }

        protected override async Task When()
        {
            try
            {
                await _sut.SendMessage(_chatRoomId, _username, _message);
            }
            catch (Exception exception)
            {
                _exception = exception;
            }
        }

        [Fact]
        public void Then_It_Should_Not_Throw_Any_Exception()
        {
            _exception.Should().BeNull();
        }
    }
}