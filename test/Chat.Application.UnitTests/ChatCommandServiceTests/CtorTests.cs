using Chat.Application.Contracts;
using Chat.Application.UnitTests.TestSupport;
using FluentAssertions;
using Moq;

namespace Chat.Application.UnitTests.ChatCommandServiceTests;

public static class CtorTests
{
    public class Should_Instantiate_ChatCommandService
        : Given_When_Then
    {
        private ChatCommandService _sut = null!;

        protected override void Given()
        {
        }

        protected override void When()
        {
            _sut = new ChatCommandService(Mock.Of<IChatRoomRepository>(), Mock.Of<IChatRoomFactory>());
        }
        
        [Fact]
        public void Then_It_Should_Have_A_Valid_Instance()
        {
            _sut.Should().NotBeNull();
        }
    }
}   