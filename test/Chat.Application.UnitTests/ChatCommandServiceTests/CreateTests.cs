using Chat.Application.Contracts;
using Chat.Application.UnitTests.TestSupport;
using Chat.Application.UnitTests.TestSupport.Builders;
using Chat.Domain;
using FluentAssertions;
using Moq;

namespace Chat.Application.UnitTests.ChatCommandServiceTests;

public static class CreateTests
{
    public class Should_Create_And_Save_A_ChatRoom
        : Given_When_Then_Async
    {
        private Mock<IChatRoomRepository> _chatRoomRepositoryMock = null!;
        private ChatCommandService _sut = null!;
        private Guid _result;

        protected override Task Given()
        {
            _chatRoomRepositoryMock = new Mock<IChatRoomRepository>();
            var idFactory = new IdFactory();
            
            var chatRoomFactory =
                new ChatRoomFactoryBuilder()
                    .WithIdFactory(idFactory)
                    .Object;
            
            _sut =
                new ChatCommandServiceBuilder()
                    .WithChatRoomRepository(_chatRoomRepositoryMock.Object)
                    .WithChatRoomFactory(chatRoomFactory)
                    .Object;
            return Task.CompletedTask;
        }

        protected override async Task When()
        {
            _result = await _sut.Create();
        }

        [Fact]
        public void Then_It_Should_Return_A_Valid_Id()
        {
            _result.Should().NotBeEmpty();
        }

        [Fact]
        public void Then_It_Should_Save_The_ChatRoom()
        {
            _chatRoomRepositoryMock.Verify(x => x.Save(It.IsAny<ChatRoom>()), Times.Once);
        }
    }
    
    public class Should_Create_And_Save_A_Unique_ChatRoom
        : Given_When_Then_Async
    {
        private Mock<IChatRoomRepository> _chatRoomRepositoryMock = null!;
        private ChatCommandService _sut = null!;
        private Guid _result;
        private Guid _oldId;

        protected override async Task Given()
        {
            _chatRoomRepositoryMock = new Mock<IChatRoomRepository>();
            var idFactory = new IdFactory();
            
            var chatRoomFactory =
                new ChatRoomFactoryBuilder()
                    .WithIdFactory(idFactory)
                    .Object;
            
            _sut =
                new ChatCommandServiceBuilder()
                    .WithChatRoomRepository(_chatRoomRepositoryMock.Object)
                    .WithChatRoomFactory(chatRoomFactory)
                    .Object;

            _oldId = await _sut.Create();
        }

        protected override async Task When()
        {
            _result = await _sut.Create();
        }

        [Fact]
        public void Then_It_Should_Return_A_Different_Id()
        {
            _result.Should().NotBe(_oldId);
        }
    }
}