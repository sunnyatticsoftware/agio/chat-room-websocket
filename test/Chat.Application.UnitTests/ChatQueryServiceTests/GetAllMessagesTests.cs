using Chat.Application.Contracts;
using Chat.Application.Models;
using Chat.Application.UnitTests.TestSupport;
using Chat.Application.UnitTests.TestSupport.Builders;
using Chat.Application.UnitTests.TestSupport.Extensions;
using Chat.Domain;
using Chat.Domain.Contracts;
using FluentAssertions;
using Moq;

namespace Chat.Application.UnitTests.ChatQueryServiceTests;

public static class GetAllMessagesTests
{
    public class Should_Get_No_Messages_When_There_Are_Not_Messages
        : Given_When_Then_Async
    {
        private ChatQueryService _sut = null!;
        private Guid _chatRoomId;
        private IEnumerable<MessageDto> _result = null!;

        protected override Task Given()
        {
            _chatRoomId = 1.ToGuid();
            
            var chatRoom = new ChatRoom(_chatRoomId, Mock.Of<IDateTimeFactory>(), Mock.Of<INotificationService>());
            var chatRoomRepositoryMock = new Mock<IChatRoomRepository>();
            chatRoomRepositoryMock
                .Setup(x => x.GetById(_chatRoomId))
                .ReturnsAsync(chatRoom);
            
            _sut = 
                new ChatQueryServiceBuilder()
                    .WithChatRoomRepository(chatRoomRepositoryMock.Object)
                    .Object;
            
            return Task.CompletedTask;
        }

        protected override async Task When()
        {
            _result = await _sut.GetAllMessages(_chatRoomId);
        }

        [Fact]
        public void Then_It_Should_Return_Empty_List()
        {
            _result.Should().BeEmpty();
        }
    }
    
    public class Should_Get_All_Messages_From_Different_Users_Who_Sent_Messages
        : Given_When_Then_Async
    {
        private ChatQueryService _sut = null!;
        private Guid _chatRoomId;
        private IEnumerable<MessageDto> _result = null!;
        private IEnumerable<MessageDto> _expectedMessages = null!;

        protected override Task Given()
        {
            _chatRoomId = 1.ToGuid();

            var dateOne = 1.ToUtcDate();
            var dateTwo = 2.ToUtcDate();
            var dateThree = 3.ToUtcDate();

            var dateTimeFactoryMock = new Mock<IDateTimeFactory>();
            dateTimeFactoryMock
                .SetupSequence(x => x.CreateUtcNow())
                .Returns(dateOne)
                .Returns(dateTwo)
                .Returns(dateThree);

            var chatRoom = new ChatRoom(_chatRoomId, dateTimeFactoryMock.Object, Mock.Of<INotificationService>());

            var userOne = new User("one", "connection-one");
            var userTwo = new User("two", "connection-two");
            var userThree = new User("three", "connection-three");
            
            chatRoom.Join(userOne);
            chatRoom.Join(userTwo);
            chatRoom.Join(userThree);

            var messageOne = "message one";
            var messageTwo = "message two";
            var messageThree = "message three";

            chatRoom.SendMessage(userOne.Username, messageOne);
            chatRoom.SendMessage(userTwo.Username, messageTwo);
            chatRoom.SendMessage(userThree.Username, messageThree);

            var chatRoomRepositoryMock = new Mock<IChatRoomRepository>();
            chatRoomRepositoryMock
                .Setup(x => x.GetById(_chatRoomId))
                .ReturnsAsync(chatRoom);
            
            _sut = 
                new ChatQueryServiceBuilder()
                    .WithChatRoomRepository(chatRoomRepositoryMock.Object)
                    .Object;

            _expectedMessages =
                new List<MessageDto>
                {
                    new(dateOne, userOne.Username, messageOne, $"[{dateOne.ToString("yyyy-MM-dd HH:mm:ss")}] {userOne.Username} says: {messageOne}"),
                    new(dateTwo, userTwo.Username, messageTwo, $"[{dateTwo.ToString("yyyy-MM-dd HH:mm:ss")}] {userTwo.Username} says: {messageTwo}"),
                    new(dateThree, userThree.Username, messageThree, $"[{dateThree.ToString("yyyy-MM-dd HH:mm:ss")}] {userThree.Username} says: {messageThree}")
                };
            
            return Task.CompletedTask;
        }

        protected override async Task When()
        {
            _result = await _sut.GetAllMessages(_chatRoomId);
        }

        [Fact]
        public void Then_It_Should_Return_Expected_Messages()
        {
            _result.Should().BeEquivalentTo(_expectedMessages);
        }
    }
}