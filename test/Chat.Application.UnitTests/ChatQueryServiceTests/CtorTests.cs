using Chat.Application.Contracts;
using Chat.Application.UnitTests.TestSupport;
using FluentAssertions;
using Moq;

namespace Chat.Application.UnitTests.ChatQueryServiceTests;

public static class CtorTests
{
    public class Should_Instantiate_ChatQueryService
        : Given_When_Then
    {
        private ChatQueryService _sut = null!;

        protected override void Given()
        {
        }

        protected override void When()
        {
            _sut = new ChatQueryService(Mock.Of<IChatRoomRepository>());
        }

        [Fact]
        public void Then_It_Should_Have_Valid_Instance()
        {
            _sut.Should().NotBeNull();
        }
    }
}