using Chat.Application.Contracts;
using Moq;

namespace Chat.Application.UnitTests.TestSupport.Builders;

public class ChatQueryServiceBuilder
{
    private IChatRoomRepository _chatRoomRepository = Mock.Of<IChatRoomRepository>();

    public ChatQueryServiceBuilder WithChatRoomRepository(IChatRoomRepository chatRoomRepository)
    {
        _chatRoomRepository = chatRoomRepository;
        return this;
    }

    public ChatQueryService Object => new ChatQueryService(_chatRoomRepository);
}