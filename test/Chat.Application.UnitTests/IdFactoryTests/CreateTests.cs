using Chat.Application.UnitTests.TestSupport;
using FluentAssertions;

namespace Chat.Application.UnitTests.IdFactoryTests;

public static class CreateTests
{
    public class Should_Create_Valid_Guid
        : Given_When_Then
    {
        private IdFactory _sut = null!;
        private Guid _result;

        protected override void Given()
        {
            _sut = new IdFactory();
        }

        protected override void When()
        {
            _result = _sut.Create();
        }

        [Fact]
        public void Then_It_Should_Create_Valid_Guid()
        {
            _result.Should().NotBeEmpty();
        }
    }
    
    public class Should_Create_Unique_Guid
        : Given_When_Then
    {
        private IdFactory _sut = null!;
        private Guid _result;
        private Guid _oldResult;

        protected override void Given()
        {
            _sut = new IdFactory();
            _oldResult = _sut.Create();
        }

        protected override void When()
        {
            _result = _sut.Create();
        }

        [Fact]
        public void Then_It_Should_Create_Unique_Guid()
        {
            _result.Should().NotBe(_oldResult);
        }
    }
}