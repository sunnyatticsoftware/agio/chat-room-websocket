using Chat.Domain.Contracts;
using Moq;

namespace Chat.Domain.UnitTests.TestSupport.Builders;

public class ChatRoomBuilder
{
    private IDateTimeFactory _dateTimeFactory = Mock.Of<IDateTimeFactory>();
    private INotificationService _notificationService = Mock.Of<INotificationService>();
    private Guid _id = Guid.Empty;

    public ChatRoomBuilder WithDateTimeFactory(IDateTimeFactory dateTimeFactory)
    {
        _dateTimeFactory = dateTimeFactory;
        return this;
    }

    public ChatRoomBuilder WithNotificationService(INotificationService notificationService)
    {
        _notificationService = notificationService;
        return this;
    }

    public ChatRoomBuilder WithId(Guid id)
    {
        _id = id;
        return this;
    }
    
    public ChatRoom Object => new(_id, _dateTimeFactory, _notificationService);
}