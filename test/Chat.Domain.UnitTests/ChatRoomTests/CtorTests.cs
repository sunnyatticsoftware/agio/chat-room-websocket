using Chat.Domain.Contracts;
using Chat.Domain.UnitTests.TestSupport;
using FluentAssertions;
using Moq;

namespace Chat.Domain.UnitTests.ChatRoomTests;

public static class CtorTests
{
    public class Should_Instantiate_ChatRoom
        : Given_When_Then
    {
        private ChatRoom _sut = null!;

        protected override void Given()
        {
        }

        protected override void When()
        {
            _sut = new ChatRoom( Guid.Empty, Mock.Of<IDateTimeFactory>(), Mock.Of<INotificationService>());
        }

        [Fact]
        public void Then_It_Should_Have_A_Valid_Instance()
        {
            _sut.Should().NotBeNull();
        }
    }
}