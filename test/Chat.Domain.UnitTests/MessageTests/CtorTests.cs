using Chat.Domain.UnitTests.TestSupport;
using FluentAssertions;

namespace Chat.Domain.UnitTests.MessageTests;

public static class CtorTests
{
    public class Should_Instantiate_Message
        : Given_When_Then
    {
        private Message _sut = null!;
        private DateTime _createdOn;
        private string _createdBy = null!;
        private string _text = null!;

        protected override void Given()
        {
            _createdOn = DateTime.MaxValue;
            _createdBy = "foo";
            _text = "hello";
        }

        protected override void When()
        {
            _sut = new Message(_createdOn, _createdBy, _text);
        }

        [Fact]
        public void Then_It_Should_Have_A_Valid_Instance()
        {
            _sut.Should().NotBeNull();
        }

        [Fact]
        public void Then_It_Should_Have_The_Expected_CreatedOn()
        {
            _sut.CreatedOn.Should().Be(_createdOn);
        }

        [Fact]
        public void Then_It_Should_Have_The_Expected_CreatedBy()
        {
            _sut.CreatedBy.Should().Be(_createdBy);
        }

        [Fact]
        public void Then_It_Should_Have_The_Expected_Text()
        {
            _sut.Text.Should().Be(_text);
        }
    }
}