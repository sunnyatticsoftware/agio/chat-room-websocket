using Chat.Domain.UnitTests.TestSupport;
using FluentAssertions;

namespace Chat.Domain.UnitTests.UserTests;

public static class EqualsTests
{
    public class Should_Be_Equal_To_Another_User_With_Same_Username_And_Connection_Id
        : Given_When_Then
    {
        private User _existingUser = null!;
        private User _sut = null!;
        private bool _result;

        protected override void Given()
        {
            var username = "foo";
            var connectionId = "bar";
            _existingUser = new User(username, connectionId);
            _sut = new User(username, connectionId);
        }

        protected override void When()
        {
            _result = _sut.Equals(_existingUser);
        }

        [Fact]
        public void Then_It_Should_Be_True()
        {
            _result.Should().BeTrue();
        }
    }
    
    public class Should_Be_Equal_To_Another_User_With_Same_Username_And_Connection_Id_When_Using_Equals_Operator
        : Given_When_Then
    {
        private User _existingUser = null!;
        private User _sut = null!;
        private bool _result;

        protected override void Given()
        {
            var username = "foo";
            var connectionId = "bar";
            _existingUser = new User(username, connectionId);
            _sut = new User(username, connectionId);
        }

        protected override void When()
        {
            _result = _sut == _existingUser;
        }

        [Fact]
        public void Then_It_Should_Be_True()
        {
            _result.Should().BeTrue();
        }
    }
}