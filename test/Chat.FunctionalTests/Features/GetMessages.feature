Feature: Get Messages
	Gets all messages in a chat room

Scenario: Gets all messages from different users in a chat
	Given there is a chat room
	And a user with username pepe and connection id connection-one joins
	And a user with username paula and connection id connection-two joins
	And a user with username leyre and connection id connection-three joins
	And the user with username leyre sends message Hello how is everyone?
	And the user with username pepe sends message I am good thanks
	When getting all messages
	Then the message 1 is from leyre and says Hello how is everyone?
	And the message 2 is from pepe and says I am good thanks