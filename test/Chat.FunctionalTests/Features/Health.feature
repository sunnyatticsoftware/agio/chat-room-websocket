Feature: Health
	Simple health check

Scenario: Check health
	Given the health url /health
	When sending GET request
	Then the response has status code 200
	And the executing environment is Test