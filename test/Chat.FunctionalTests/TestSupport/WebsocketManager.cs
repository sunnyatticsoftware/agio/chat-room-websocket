using System.Net.WebSockets;
using System.Reactive.Linq;
using System.Text.Json;
using Websocket.Client;

namespace Chat.FunctionalTests.TestSupport;

public class WebsocketManager
    : IDisposable
{
    private readonly WebsocketClient _client;

    public WebsocketManager()
    {
        var websocketUri = new Uri("ws://localhost:8080/ws");
        _client = new WebsocketClient(websocketUri);
        _client.IsReconnectionEnabled = false;
    }

    public async Task Start(
        Func<Task> onStarted,
        Func<string, Task> onConnectionIdReceived,
        Func<string, Task> onNotificationReceived)
    {
        if (_client.IsStarted)
        {
            return;
        }
        
        _client
            .DisconnectionHappened
            .Subscribe(info =>
            {
                // Do something?
            });
        
        _client
            .MessageReceived
            .Select(msg =>  Observable.FromAsync(async () =>
            {
                var json = msg.Text;
                try
                {
                    var isConnectionId =
                        JsonSerializer
                            .Deserialize<JsonElement>(json)
                            .TryGetProperty("connectionId", out var connectionIdJsonElement);
                    if (isConnectionId)
                    {
                        var connectionId = connectionIdJsonElement.GetString()!;
                        await onConnectionIdReceived.Invoke(connectionId);
                        return;
                    }
                }
                catch (Exception)
                {
                    // Deserialization exception. Do something?
                }

                await onNotificationReceived(json);
            }))
            .Concat()
            .Subscribe();

        try
        {
            await _client.StartOrFail();
            await onStarted.Invoke();
        }
        catch (Exception exception)
        {
            throw new ApplicationException("Unable to connect to websocket", exception);
        }
    }

    public async Task Send(string json)
    {
        await Task.Run(() => _client.Send(json));
    }
    
    public async Task Close()
    {
        await _client.Stop(WebSocketCloseStatus.NormalClosure, "Closed");
        Dispose();
    }

    public void Dispose()
    {
        _client.Dispose();
    }
}