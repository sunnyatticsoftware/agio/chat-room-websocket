namespace Chat.FunctionalTests.TestSupport;

public abstract class Given_When_Then
    : IDisposable
{
    public void Dispose()
    {
        Cleanup();
    }

    protected Given_When_Then()
    {
        Setup();
    }

    private void Setup()
    {
        Given();
        When();
    }

    protected abstract void Given();

    protected abstract void When();

    protected virtual void Cleanup()
    {
    }
}