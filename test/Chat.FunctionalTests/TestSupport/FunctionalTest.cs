using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;

namespace Chat.FunctionalTests.TestSupport;

public abstract class FunctionalTest
{
    protected HttpClient HttpClient { get; }
    
    public FunctionalTest()
    {
        var server =
            new TestServer(
                new WebHostBuilder()
                    .UseStartup<Startup>()
                    .UseEnvironment("Test")
                    .UseCommonConfiguration()
                    .ConfigureTestServices(ConfigureTestServices));

        HttpClient = server.CreateClient();
    }

    protected virtual void ConfigureTestServices(IServiceCollection services)
    {
    }
}