# chat-room

El ejercicio consiste en implementar, con TDD (inside-out) y un diseño de arquitectura limpia, una aplicación software en la que se puedan crear salones de chat y diferentes usuarios puedan añadir y recibir mensajes en tiempo real, así como consultar el histórico de mensajes.

![Diagrama de chats](doc/chat-diagram.png)

Se comenzará diseñando la capa de dominio, se continuará por la capa de aplicación y, finalmente, se llegará a la capa de infraestructura donde se expondrá la funcionalidad por http y se logrará notificaciones en tiempo real con un sistema de websocket externo que no hace falta construir y que funciona a modo similar a como lo hace AWS Api Gateway con websockets.

## Pre-requisitos
### Herramienta wscat para cliente websocket
Instalar NodeJs y NPM. Después
```
npm install -g wscat
```

Más info: https://github.com/websockets/wscat

### Utilidad API Gateway websocket
AWS tiene un API Gateway que soporta protocolos HTTP y Websocket.
Vamos a imaginar que disponemos de una cuenta en AWS y una instancia API Gateway configurada para ser utilizada con websockets a la que nos podemos conectar con websockets y recibir mensajes en tiempo real. Esta herramienta se comporta de forma parecida a AWS Api Gateway para mantener una lista de conexiones y enviarles mensajes.

```
docker run -p 8080:8080 registry.gitlab.com/sunnyatticsoftware/sasw-community/sasw-websocket-tester:latest
```

Se puede conectar con
```
wscat -c ws://localhost:8080/ws

> foo
< {"statusCode":200,"connectionId":"klHkqyncJA"}
```

y para enviar mensajes con un connection Id:
```
curl -d '{"key1":"value1", "key2":"value2"}' -H "Content-Type: application/json" -X POST http://localhost:8080/@connections/klHkqyncJA
```
